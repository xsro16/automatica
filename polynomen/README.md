# Polynomen - Polynomial Library

[Home page and software specification](https://daingun.gitlab.io/automatica/polynomen)

[Repository](https://gitlab.com/daingun/automatica)

[Crate registry](https://crates.io/crates/polynomen)

[Documentation](https://docs.rs/polynomen)

Polynomial creation from coefficients or roots.  
Polynomial evaluation with Horner method.  
(Mutable) Indexing of polynomial coefficients.  
Polynomials addition, subtraction and division.  
Polynomials multiplication with convolution and fast fourier transform.  
Polynomial and scalar addition, subtraction, multiplication and division.  
Polynomial roots finding (real and complex).  
Polynomial differentiation and integration.  

## Examples

```
#[macro_use] extern crate polynomen;
use polynomen::Poly;
let p1 = poly!(1, 2, 3);
let p2 = Poly::new_from_coeffs(&[1, 2, 3]);
assert_eq!(p1, p2);
```

Examples of library usage can be found in the `examples/` folder.
