#!/usr/bin/env sh

# run as:
#    ./update-version.sh folder/ 0.10.0

if [ ! -d "$1" ]; then
    echo "$1 is not a directory"
    exit 1
fi

cd "$1" || exit 1
sed -E -i \
    "s/^version = \"[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+\"/version = \"$2\"/" \
    Cargo.toml

if [ -d design/ ]; then
    sed -E -i \
        "s/^version: [[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+/version: $2/" \
        design/*.md
fi
exit 0
