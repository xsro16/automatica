---
title: Polynomen
version: 1.0.0
license: CC BY-SA 4.0
---

# Polynomial Library

This library contains also a module for the creation and manipulation of polynomials, for which all the arithmetical operations, the derivative, the integration, the evaluation with real and complex numbers are defined, real and complex roots can be calculated.

# Introduction

## Purpose

This SRS (Software Requirements Specification) has the purpose of supplying the reference for the high level description of the software, of its design and its verification.

This document is addressed to those that intend to use this library in their software or those that intend to contribute to the development of the library.

## Scope

The name of the library is `polynomen`. The chosen programming language for the development is `Rust` [8].

The version control system is `git` [9] and the source code is hosted on `Gitlab` at the following link:

<https://gitlab.com/daingun/automatica>

The access to the public registry crates.io [10] is available at the following link:

<https://crates.io/crates/polynomen>

The library supplies an infrastructure that contains calculation methods for polynomials. This software does not supply an interface with the final user.

## Definitions, acronyms and abbreviations

*Polynomial*: algebraic sum of monomials made by a coefficient and a literal part;

*by\_value*: the method parameter is passed as value;

*by\_ref*: the method parameter is passed as reference;

*in\_place*: the method parameter is passed as mutable reference;

*FR*: Functional requirement;

*TC*: Test case.

## Overview

This SRS is subdivided in a general description of the library that contains the interfaces and the requirements for the software, a description of specific and functional requirements of the library that describe what it is able to execute, a description of the test plan both black box and white box type, a description of the continuous integration process.

# Overall description

## Product perspective

### System interfaces

The library can be used on platforms on which the support for compilation exists.
For a reference to the available platforms it is possible to refer to the official list [11].

### User interfaces

This library is not designed to have a direct interaction with the user, therefore there are not user interfaces.

### Hardware Interfaces

This library does not directly interact with the hardware, it uses the function made available by the `Rust` standard library and the operating system in use.

### Software interfaces

The library needs the `rustc` compiler for the compilation with a version equal of greater than 1.56.

The list of the needed dependencies for the compilations of the library is the following:

- nalgebra
    * linear algebra, matrices, operations on matrices, decomposition and eigenvalues calculation
    * source: <https://crates.io/crates/nalgebra>
    * version: 0.23
- complex-division
    * algorithm for complex numbers division
    * source: <https://crates.io/crates/complex-division>
    * version: 1.0
- zip-fill
    * iterator extensions
    * source: <https://crates.io/crates/zip-fill>
    * version: 1.0

The following dependencies are necessary for the development phase:

- approx
    * approximate equality for floating point numbers
    * source: <https://crates.io/crates/approx>
    * version: 0.4
- proptest
    * randomized tests
    * source: <https://crates.io/crates/proptest>
    * version: 0.10

### Communications interfaces

This library does not have network interfaces.

### Memory

There are not memory limits for the use of this library.

## Product functions

The library allows the calculation of polynomials, it includes the arithmetical operations and the methods for the calculation of the roots of the polynomial.

## Constraints

There are non constraints in the use of this library.
Regarding the development, the correctness must be the first aim.

# Specific requirements

## External interfaces

### Data structures

This library must present as interface structures of the `Rust` standard library, or structure created by this library.

Using as interface as structure defined in a dependency exposes the internal implementation ad forces the use of that dependency.

When this library must expose traits from its dependencies its shall re-export those crates.

## Functional requirements

### Polynomial creation

*FR.1* The user shall be able to create polynomials with real coefficients, supplying the coefficients of the roots, both as list or as iterator.

*FR.2* In particular the coefficients shall be supplied from the lowest to the highest degree monomial. It is necessary to put zeros where the monomial is null.

*FR.3* Both the null polynomial and the unity polynomial can be created.

### Indexing

*FR.4* The coefficients can be indexed, the index of the monomial is equal to its degree. The indexed coefficient shall be modifiable.

### Properties

It is possible to operate on polynomial properties:

*FR.5* Degree calculation (it is undefined for null polynomial) (by ref);

*FR.6* Extension of the polynomial with zeros in the highest degree monomials (in place);

*FR.7* The transformation into a monic polynomial and the return of the leading coefficient (in place, by ref);

*FR.8* Evaluation of a polynomial both with real and complex numbers, as well as other polynomials, using Horner's method (by value, by ref);

*FR.9* Rounding towards zero of the coefficients given an absolute tolerance (by ref, in place).

### Roots

*FR.10* It is possible to calculate the roots of a polynomial. In the case of complex roots both the eigenvalues and iterative [1][2][3][4][6][7] methods are available. In the case the user needs real roots, a result is supplied (with eigenvalues method) only in the case that all roots are real.

### Arithmetical operations and infinitesimal calculus

*FR.11* On polynomial it is possible to perform arithmetical operations, both between polynomials and scalars, and operations of infinitesimal calculus:

- negation of the polynomial (by value, by ref)
- addition, subtraction, division and division reminder [5] between polynomials (by value, by ref)
- multiplication between polynomials both with the convolution method and the fast Fourier transform method [6] (by value, by ref)
- addition, subtraction, multiplication, and division with a scalar (by value, by ref)
- calculation of the derivative and the integral of the polynomial (by ref)
- evaluation of polynomial ratios avoiding overflows (by ref)
- exponentiation with a positive exponent (by ref)

### Formatting

*FR.12* It is available a standard formatting for the output of the polynomial as string.

### Polynomial norms

*FR.13* The calculation of the following norms of the polynomial shall be available:

- l1-norm: the sum of the absolute values of the coefficients
- l2-norm: the square root of the sum of the squares of the coefficients
- l∞-norm: the maximum of the absolute values of the coefficients

### Polynomial greatest common divisor

*FR.14* The calculation of the greater common divisor. The result is not required to be in monic form.

# Test Plan

## Black box testing

Black box tests verify the properties of the mathematical entities defined in the library.

Tests have defined the path in the test files, the inputs and the expected outputs.

*TC.1* A polynomial multiplied by one returns the same polynomial.

    tests/polynomial/multiplicative_unity

- input: p1 (arbitrary polynomial, null polynomial), p2 (unity polynomial, scalar 1)
- output: p1

*TC.2* A polynomial multiplied by zero returns zero.

    tests/polynomial/multiplicative_null

- input: p1 (arbitrary polynomial, null polynomial), p2 (null polynomial, scalar 0)
- output: null polynomial

*TC.3* If a non-null polynomial is multiplied by another polynomial and divided by the same, the result is equal to the first polynomial.

    tests/polynomial/multiplicative_inverse

- input: p1 (arbitrary polynomial), p2 (arbitrary polynomial)
- output: p1

*TC.4* A polynomial plus or minus zero is equal the the polynomial itself.

    tests/polynomial/additive_invariant

- input: p1 (arbitrary polynomial), p2 (null polynomial, scalar 0)
- output: p1

*TC.5* If to a polynomial is added and subtracted another polynomial, the result is equal to the first polynomial.

    tests/polynomial/additive_inverse

- input: p1 (arbitrary polynomial), p2 (arbitrary polynomial)
- output: p1

*TC.6* The number of roots of a polynomial is equal to its degree.

    tests/polynomial/roots_degree

- input: $1, 1+2x, 1+2x+3x^{2}$
- output: Some(0), Some(1), Some(2)

*TC.7* A null polynomial does not have a defined degree.

    tests/polynomial/no_degree

- input: null polynomial
- output: None

*TC.8* The derivative of a polynomial has one less degree than the starting polynomial.

    tests/polynomial/derivation

- input: $2x+3x^{2}, 2+6x, 6$
- output: Some(1), Some(0), None

*TC.9* The integral of a polynomial has one more degree than the starting polynomial.

    tests/polynomial/integration

- input: $0, -1+x, 2-x+\frac{x^{2}}{2}$
- output: Some(0), Some(1), Some(2)

*TC.10* The stationary points of a polynomial function are the roots of its derivative.

    tests/polynomial/maximum_minimum

- input: $\left(1+x\right)x\left(-1+x\right)$
- output: stationary points in $x=\pm 0.57735$

*TC.11* The iterative root finding method must be stable for nearly multiple zeros ([13] page 6).

    tests/polynomial/nearly_multiple_zeros

- input: P4, P5, P6, P8
- output: roots must all be real, from root it is possible to recover the original polynomial with given error or roots must be within given error.

*TC.12* The iterative root finding method must be stable for equimodular zeros ([13] page 7).

    tests/polynomial/equimodular_zeros

- input: P9
- output: there shall be 20 roots, all root must have a modulus of either 100 or 0.01

*TC.13* Test defects in the root finding algorithm ([13] page 8).

    tests/polynomial/defects_in_algorithm

- input: P10
- output: all roots must be equal to the one use to build the polynomials

*TC.14* Test defects in the root finding algorithm by calculating roots on a unit circle and a circle of radius 0.9 ([13] page 8).

    tests/polynomial/defects_on_circle

- input: P11
- output: all roots must have modulus of either 1.0 or 0.9

## White box testing

Every module must contain a test submodule, whose purpose it to test all the functions present inside the module.

This tests are dependent from the implementation of the method, it is not given a detailed description, but the requirements are listed and what the test shall verify. More information must be added in the source code.

### Generic methods

Formatting polynomial as string.

Creation from coefficient and from roots, supplied both as a list and as a iterator, creation of the null and unity polynomial.

Return coefficients as a list.

Calculation of the length and the degree of the polynomial.

Extension with zeros of the high degree coefficients.

Evaluation of the polynomial both with real and complex numbers.

Indexing of polynomial coefficients.

Derivative and integral of the polynomial.

Transformation into a monic polynomial and return of the leading coefficient.

Rounding towards zero of the coefficients.

Creation of the companion matrix.

Polynomial norms.

### arithmetic

Arithmetic operations defined between polynomial and with real numbers.

Evaluation of polynomial ratios without overflows.

Multiplication through fast Fourier transformation.

Greatest common divisor.

### fft

Bit permutation of a integer number.

Calculation of the direct and inverse fast Fourier transformation.

### roots

Calculation of the real and complex roots of a polynomial of any degree, both with the method of eigenvalues and the iterative method. The null roots shall be removed from the calculation.

### convex hull

Top convex hull of a set of points. Vectors turns and vectors cross product.

## Documentation tests

Every public method of the library shall have a documentation in the source code, in addition there shall be an example of the use of the method.

This example will compiled and executed during the test phase, ensuring that the documentation remains aligned with the code modifications.

## Test coverage

The test development shall tend to obtain the maximum line coverage.

The program to determine the test coverage is `tarpaulin` [12], it is executed during the continuous integration phases.

# Continuous integration

## Description

The use of GitLab platform for the management of the source code of the library, allows the management of the continuous integration activities.

The process of continuous integration allows to have the certainty that, after changes to the library, the code can compile and that all tests are executed.

The pipeline is executed every time that the changes to the source code are pushed to the repository. Failures during the process are reported by email.

## Structure

The pipeline is made by the following list of stage and jobs:

1. build
  - *build:oldest* library compilation with the minimum supported `Rust` version, both in debug and release mode
  - *build:latest* library compilation with the latest `Rust` version, both in debug and in release mode, it is executed also in merge request events

2. test
  - *test:run* execution of all tests present in the library with the latest `Rust` version, both in debug and in release mode, it is executed also in merge request events
  - *cover* execution of test coverage program `tarpaulin`

3. examples
  - *examples:run* compilation and execution of the examples of the library using the latest `Rust` version

4. package
  - *package:build* verification of the creation of the library package

5. doc
  - *pages* creation of documentation pages in HTML format and publishing through the GitLab pages service, this job is executed only on master branch and its activation is manual

6. publish
  - *publish:send* creation of publishing on the public registry of the library package, this job is executed only on master branch and its activation is manual

# References

[1] O. Aberth, Iteration Methods for Finding all Zeros of a Polynomial Simultaneously, Math. Comput. 27, 122 (1973) 339–344.

[2] D. A. Bini, Numerical computation of polynomial zeros by means of Aberth's method, Baltzer Journals, June 5, 1996

[3] D. A. Bini, L. Robol, Solving secular and polynomial equations: A multiprecision algorithm, Journal of Computational and Applied Mathematics (2013)

[4] W. S. Luk, Finding roots of real polynomial simultaneously by means of Bairstow's method, BIT 35 (1995), 001-003

[5] Donald Ervin Knuth, The Art of Computer Programming: Seminumerical algorithms, Volume 2, third edition, section 4.6.1, Algorithm D: division of polynomials over a field

[6] T. H. Cormen, C. E. Leiserson, R. L. Rivest, C. Stein, Introduction to Algorithms, 3rd edition, McGraw-Hill Education, 2009

[7] A. M. Andrew, Another Efficient Algorithm for Convex Hulls in Two Dimensions, Info. Proc. Letters 9, 216-219 (1979)

[8] <https://www.rust-lang.org/>

[9] <https://git-scm.com/>

[10] <https://crates.io/>

[11] <https://forge.rust-lang.org/release/platform-support.html>

[12] <https://crates.io/crates/cargo-tarpaulin>

[13] Jenkins, M. & Traub, Joseph. (1975). Principles for Testing Polynomial Zerofinding Programs. ACM Transactions on Mathematical Software (TOMS). 1. 26-34. 10.1145/355626.355632.
