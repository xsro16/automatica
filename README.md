# Workspace

## Automatica

Automatic control systems library.
[Crate registry (https://crates.io/crates/automatica)](https://crates.io/crates/automatica)

## Polynomen

Polynomial Library.
[Crate registry (https://crates.io/crates/polynomen)](https://crates.io/crates/polynomen)

## Complex-division

Library for the calculation of complex number division.
[Crate registry (https://crates.io/crates/complex_division)](https://crates.io/crates/complex_division)
